export default class StateLoader {
  loadState() {
    try {
      const serializedState = localStorage.getItem('userState');
      if (serializedState === null) {
        return this.initializeState();
      }

      return JSON.parse(serializedState);
    } catch (err) {
      return this.initializeState();
    }
  }

  saveState(state) {
    try {
      const global = state.toJS().global;
      const serializedState = JSON.stringify({ global });
      localStorage.setItem('userState', serializedState);
    } catch (err) {}
  }

  initializeState() {
    return {
      // state object
    };
  }
}
