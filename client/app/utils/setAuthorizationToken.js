import axios from 'axios';

export default function (token) {
  axios.defaults.baseURL = 'http://localhost:1337';

  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  } else {
    // axios.defaults.headers.common['Authorization'] = null;
    delete axios.defaults.headers.common.Authorization;
    /* if setting null does not remove `Authorization` header then try
          delete axios.defaults.headers.common['Authorization'];
        */
  }
}

