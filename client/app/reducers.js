/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { fromJS } from 'immutable';
import { combineReducers } from 'redux-immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form/immutable';
import { reducer as toastrReducer } from 'react-redux-toastr';

import globalReducer from 'containers/Login/reducer';
import { LOGOUT_SUCCESS } from './containers/Login/constants';
// import { LOGOUT_SUCCESS } from './containers/LoginForm/constants';
/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@5
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  location: null,
});

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        location: action.payload,
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the dynamically injected ones
 */

export const makeRootReducer =
(injectedReducers) =>
  (state, action) =>
    combineReducers({
      route: routeReducer,
      global: globalReducer,
      form: formReducer,
      toastr: toastrReducer,
      ...injectedReducers,
    })(action.type === LOGOUT_SUCCESS ? undefined : state, action);


export default makeRootReducer;
