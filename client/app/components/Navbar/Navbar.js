import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../Button';
import NavLink from '../NavLink';

const Navbar = ({ user = {}, logout }) => (
  <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    <Link className="navbar-brand" to="/">Simple Timeline App</Link>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon" />
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <NavLink className="nav-link" to="/">Home <span className="sr-only">(current)</span></NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" to="/add">Add Timeline</NavLink>
        </li>
      </ul>
      <ul className="navbar-nav">
        <li className="nav-item">
          <p className="navbar-text m-0 pr-4">{user.fullName}</p>
        </li>
        <li className="nav-item">
          <Button color="danger" size="md" text="Logout" type="button" onClick={logout}></Button>
        </li>
      </ul>
    </div>
  </nav>
);

export default Navbar;
