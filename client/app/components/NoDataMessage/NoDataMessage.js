import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { BarsLoadingIndicator } from '../../components/LoadingIndicator';

const NoDataMessage = ({
  message = 'No Data',
  className = '',
  isDarkText = false,
  isLoading = false,
  ...rest
}) => (
  <div className={`row h-100px ${className}`}>
    <div
      className={`col d-flex px-1 flex-column justify-content-center align-items-center ${isDarkText ? 'text-secondary' : 'text-light'}`}
      {...rest}
    >
      { isLoading ? <BarsLoadingIndicator /> : (
        <Fragment>
          <div className="text-1 center line-height-xs">
            {message}
          </div>
        </Fragment>
      )}
    </div>
  </div>
);

NoDataMessage.propTypes = {
  message: PropTypes.string,
  className: PropTypes.string,
  isDarkText: PropTypes.bool,
  isLoading: PropTypes.bool,
};

export default NoDataMessage;
