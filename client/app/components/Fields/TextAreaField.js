import React from 'react';

const TextAreaField = ({
  className = '',
  id,
  input,
  label,
  type,
  rows,
  placeholder,
  hideLabel,
  meta: { touched, error }
}) => (
  <div className={`form-group `}>
    {!hideLabel && <label htmlFor={id} className='control-label'>{label}</label>}
    <div>
      <textarea
        {...input}
        rows={rows}
        id={id}
        placeholder={placeholder || label}
        type={type}
        className={`form-control ${className} ${touched && error ? 'is-invalid' : ''}`}
      />
      {touched && error && <label className="text-danger badge">{error}</label>}
    </div>
  </div>
);

export default TextAreaField;
