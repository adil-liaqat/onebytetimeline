import React from 'react';

const InputField = ({
  className = '',
  id,
  input,
  readonly = false,
  readOnly = false,
  label,
  showLabel = true,
  type,
  meta: { touched, error }
}) => (
  <div className={`form-group`}>
    {label && showLabel && <label htmlFor={id} className='control-label'>{label}</label>}
    <div>
      <input
        {...input}
        id={id}
        placeholder={label}
        type={type}
        className={`form-control ${className} ${touched && error ? 'is-invalid' : ''}`}
        readOnly={readonly || readOnly}
      />
      {touched && error && <label className="text-danger badge">{error}</label>}
    </div>
  </div>
);

export default InputField;
