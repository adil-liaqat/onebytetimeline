import React from 'react';
import ContentLoader, { BulletList } from 'react-content-loader';
import './_style.scss';

export default (props) => (
  <div className="text-input__loading">
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
    <div className="text-input__loading--line"></div>
  </div>
);


export const ImageLoader = (props) => (
  <ContentLoader
    height={160}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#dddbdb"
    {...props}
  >
    <circle cx="199.51" cy="70.51" r="57.88" />
  </ContentLoader>
);


export const LineChartLoader = (props) => (
  <ContentLoader
    rtl
    height={160}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#dddbdb"
    {...props}
  >
    <rect x="139.11" y="99.17" rx="0" ry="0" width="120.08" height="2.67" />
    <rect x="138.38" y="39.24" rx="0" ry="0" width="3.2" height="62" />
    <rect x="130.38" y="58.24" rx="0" ry="0" width="0" height="0" />
    <rect x="150.12" y="69.2" rx="0" ry="0" width="17" height="1.88" transform="rotate(314.06, 150.12, 69.2)" />
    <rect x="159.38" y="63.24" rx="0" ry="0" width="0" height="0" />
    <rect x="162.79" y="57.55" rx="0" ry="0" width="21.77" height="1.88" transform="rotate(56.02, 162.79, 57.55)" />
    <rect x="173.97" y="74.19" rx="0" ry="0" width="18.77" height="1.88" transform="rotate(320.31, 173.97, 74.19)" />
    <rect x="189.02" y="62.27" rx="0" ry="0" width="27.5" height="1.88" transform="rotate(62.38, 189.02, 62.27)" />
    <rect x="213.92" y="68.53" rx="0" ry="0" width="21.77" height="1.88" transform="rotate(126.73, 213.92, 68.53)" />
    <rect x="213.33" y="67.09" rx="0" ry="0" width="16.5" height="1.88" transform="rotate(62.08, 213.33, 67.09)" />
    <rect x="235.95" y="60.78" rx="0" ry="0" width="25.77" height="1.88" transform="rotate(125.8, 235.95, 60.78)" />
    <rect x="235.95" y="60.69" rx="0" ry="0" width="15.77" height="1.88" transform="rotate(51.91, 235.95, 60.69)" />
  </ContentLoader>
);
