import React from 'react';
import PropTypes from 'prop-types';

const Button = ({
  text, className, size, color, isLoading, type, isDisabled, ...rest
}) => (
  <button
    type={type}
    className={`btn btn-${color} btn-${size} ${className}`}
    disabled={isLoading || isDisabled}
    {...rest}
  >
    {
      isLoading ? 'Loading...' : text
    }
  </button>
);

Button.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  color: PropTypes.oneOf([
    'primary', 'secondary', 'default', 'warning', 'info', 'success', 'danger', 'light', 'dark'
  ]),
  size: PropTypes.oneOf([
    'lg', 'sm', 'md'
  ]),
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
};

Button.defaultProps = {
  text: '',
  color: 'primary',
  type: 'submit',
  size: 'sm',
  isLoading: false,
  isDisabled: false,
  className: ''
};

export default Button;
