import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './style.scss';

const TimelineItem = ({ title, description, createdAt }) => (
  <div className="timeline-item">
    <div className="date">{moment(createdAt).format('DD MMM, YYYY')}</div>
    <h6 className="title">{title}</h6>
    <div className="timeline-description">
      {description}
    </div>
  </div>
);

TimelineItem.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  createdAt: PropTypes.string
};

export default TimelineItem;
