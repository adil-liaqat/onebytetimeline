import React from 'react';
import './style.scss';

export default () => (
  <div className="loading-indicator">
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);

export const BarsLoadingIndicator = () => (
  <div className="bars">
    <div className="rect1"></div>
    <div className="rect2"></div>
    <div className="rect3"></div>
    <div className="rect4"></div>
    <div className="rect5"></div>
  </div>
);
