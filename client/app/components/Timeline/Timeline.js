import React from 'react';

import './style.scss';

const Timeline = (props) => (
  <section className="timeline">
    {props.children}
  </section>
);

export default Timeline;
