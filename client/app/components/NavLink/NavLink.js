import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const NavLink = (props, context) => {
  const isActive = context.router.route.location.pathname === props.to;
  const activeClass = isActive ? 'active' : '';
  const { className, children, ...rest } = props;
  return (
    <Link className={`${className} ${activeClass}`} {...rest}>
      {children}
    </Link>
  );
};

NavLink.propTypes = {
  children: PropTypes.node,
  to: PropTypes.string,
  className: PropTypes.string
};

NavLink.contextTypes = {
  router: PropTypes.object,
  children: PropTypes.node
};

export default NavLink;
