import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-xhr-backend';


i18n.use(Backend).use(LanguageDetector).init({
  // we init with resources
  backend: {
    loadPath: `/locale/{{lng}}/{{ns}}.json`,
    addPath: "/locale/add/{{lng}}/{{ns}}"
  },
  fallbackLng: 'en',
  debug: true,
  // have a common namespace used around the full app
  ns: ['translation'],
  defaultNS: 'translation',

  keySeparator: false, // we use content as keys

  interpolation: {
    formatSeparator: ','
  },
  saveMissing:true,
  saveMissingTo:"current",

  react: {
    wait: true
  }
});

export default i18n;
