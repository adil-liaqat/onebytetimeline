import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form/immutable';
import { InputField } from '../../components/Fields';
import Button from '../../components/Button';

class Login extends Component {
  componentDidMount() {

  }
  render() {
    const {
      onSubmitForm, handleSubmit, dirty, submitting, loading
    } = this.props;
    return (
      <div className="card">
        <h5 className="card-header">Login</h5>
        <div className="card-body">
          <form className="form-horizontal" onSubmit={handleSubmit(onSubmitForm)}>
            <div className="form-group">

              <Field
                name="username"
                id="username"
                type="text"
                component={InputField}
                label="Username"
              />
            </div>
            <div className="form-group">

              <Field
                name="password"
                id="password"
                type="password"
                component={InputField}
                label="Password"
              />
            </div>
            <div className="form-group last">

              <Button
                text="Sign In"
                className="float-right"
                isDisabled={submitting || !dirty}
                isLoading={loading}
              />
            </div>
          </form>

        </div>
        <div className="card-footer">
          Not Registered? <Link to="/register" className>Register here</Link>
        </div>
      </div>
    );
  }
}

export default Login;
