import {
  LOGIN_REQUEST,
  LOGIN_REQUEST_SUCCESS,
  LOGIN_REQUEST_FAILURE,
  CHECK_TOKEN,
  LOGOUT,
  LOGOUT_SUCCESS
} from './constants';

export function login() {
  return {
    type: LOGIN_REQUEST,
    isAuthenticated: false
  };
}

export function loginSuccess(user) {
  return {
    type: LOGIN_REQUEST_SUCCESS,
    user,
    isAuthenticated: true
  };
}

export function loginFailure() {
  return {
    type: LOGIN_REQUEST_FAILURE,
    isAuthenticated: false
  };
}

export function checkToken() {
  return {
    type: CHECK_TOKEN
  };
}

export function logout() {
  return {
    type: LOGOUT
  };
}
export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS
  };
}
