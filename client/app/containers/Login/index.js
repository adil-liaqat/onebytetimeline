import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';

import Login from './Login';
import { login } from './actions';
import { makeSelectLoading } from './selectors';


const validate = (values) => {
  values = values.toJS();
  const errors = {};
  // VALIDATION FOR INPUT FIELDS
  if (!values.username || !values.username.trim()) {
    errors.username = 'Username is required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.username)) {
    errors.username = 'Invalid Email';
  }
  if (!values.password || !values.password.trim()) {
    errors.password = 'Password is required';
  }
  return errors;
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
});

const mapDispatchToProps = (dispatch) => ({
  onSubmitForm: () => dispatch(login())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const LoginForm = reduxForm({
  form: 'loginForm',
  enableReinitialize: true,
  validate
});

export default compose(withConnect, LoginForm)(Login);
