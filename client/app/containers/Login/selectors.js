import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');
const selectLoginForm = (state) => (state.get('form').toJS() || {}).loginForm;

const makeSelectCurrentUser = () => createSelector(
  selectGlobal,
  (globalState) => {
    let user = globalState.get('user') || {};
    if (typeof user.toJS === 'function') user = user.toJS();
    return user;
  }
);

const makeSelectFormValues = () => createSelector(
  selectLoginForm,
  (form) => ({ ...form.values })
);

const makeSelectAuthenticated = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('isAuthenticated')
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

export {
  selectGlobal,
  makeSelectFormValues,
  makeSelectCurrentUser,
  makeSelectLoading,
  makeSelectAuthenticated
};
