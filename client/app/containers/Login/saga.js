import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects';

import { push } from 'react-router-redux';

import axios from 'axios';
import moment from 'moment';
import { toastr } from 'react-redux-toastr';

import setAuthorizationHeader from '../../utils/setAuthorizationToken';

import {
  LOGIN_REQUEST,
  CHECK_TOKEN,
  LOGOUT
} from './constants';

import * as actions from './actions';
import { makeSelectFormValues } from './selectors';

export function* login() {
  const requestURL = '/api/v1/en/user/login';

  try {
    // Call our request helper (see 'utils/request')
    const form = yield select(makeSelectFormValues());
    const {
      data
    } = yield call(axios.post, requestURL, form);

    if (data.token) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('token', data.token);
    }

    yield put(actions.loginSuccess(data.user));
    setAuthorizationHeader(data.token);
    yield put(push('/'));
  } catch ({
    response
  }) {
    let error = 'Something went wrong!';
    if (typeof response.data === 'string') {
      error = response.data;
    }
    toastr.error('Error', error);
    yield put(actions.loginFailure());
  }
}

export function* logout() {
  yield localStorage.removeItem('token');
  yield localStorage.removeItem('userState');
  setAuthorizationHeader(null);
  yield put(actions.logoutSuccess());
}

export function* checkToken() {
  try {
    const token = yield localStorage.getItem('token');
    if (token) {
      const base64Url = yield token.split('.')[1];
      const base64 = yield base64Url.replace(/-/g, '+').replace(/_/g, '/');
      const {
        exp
      } = yield JSON.parse(window.atob(base64));

      if (exp && moment().isAfter(moment.unix(exp))) {
        yield put(actions.logout());
      }
    } else {
      yield put(actions.logout());
    }
  } catch (ex) {
    yield put(actions.logout());
  }
}

export default function* loginRequest() {
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount

  yield takeLatest(CHECK_TOKEN, checkToken);
  yield takeLatest(LOGIN_REQUEST, login);
  yield takeLatest(LOGOUT, logout);
}
