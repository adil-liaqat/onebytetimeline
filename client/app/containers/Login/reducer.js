import { fromJS } from 'immutable';
import {
  LOGIN_REQUEST,
  LOGIN_REQUEST_SUCCESS,
  LOGIN_REQUEST_FAILURE,
  LOGOUT_SUCCESS
} from './constants';
import { REGISTER_REQUEST, REGISTER_REQUEST_SUCCESS, REGISTER_REQUEST_FAILURE } from '../Register/constants';


const initialState = fromJS({
  user: null,
  loading: false,
  isAuthenticated: false
});


export default function (state = initialState, action) {
  switch (action.type) {
    case REGISTER_REQUEST:
    case LOGIN_REQUEST:
      return state.set('loading', true);
    case LOGIN_REQUEST_SUCCESS:
      return state.set('user', action.user).set('isAuthenticated', true).set('loading', false);
    case REGISTER_REQUEST_SUCCESS:
    case REGISTER_REQUEST_FAILURE:
    case LOGIN_REQUEST_FAILURE:
    case LOGOUT_SUCCESS:
      return state.set('user', null).set('isAuthenticated', false).set('loading', false);
    default:
      return state;
  }
}
