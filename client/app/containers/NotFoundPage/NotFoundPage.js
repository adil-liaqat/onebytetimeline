/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';
import { Link } from 'react-router-dom';
import './_style.scss';

export default function NotFound() {
  return (
    <section className="body-error error-inside">
      <div className="center-error">
        <div className="row">
          <div className="col-lg-8">
            <div className="main-error mb-3">
              <h2 className="error-code text-dark text-center font-weight-semibold m-0">
                404 <i className="fas fa-file" />
              </h2>
              <p className="error-explanation text-center">
                We're sorry, but the page you were looking for doesn't exist.
              </p>
            </div>
          </div>
          <div className="col-lg-4">
            <h4 className="text">Here are some useful links</h4>
            <ul className="nav nav-list flex-column primary">
              <li className="nav-item">
                <Link className="nav-link" to={'/'}>
                  <i className="fas fa-caret-right text-dark" /> Dashboard
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
}
