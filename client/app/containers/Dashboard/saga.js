import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects';


import axios from 'axios';

import { TIMELINE_REQUEST } from './constants';

import { makeSelectTimelineFetched } from './selectors';

import * as actions from './actions';

export function* getTimelines(res) {
  const { start, loadMore } = res;

  const requestURL = `/api/v1/en/user/timeline?start=${start}`;
  try {
    const isFetched = yield select(makeSelectTimelineFetched());
    if (!isFetched || loadMore) {
      const {
        data
      } = yield call(axios.get, requestURL);
      yield put(actions.timelineSuccess(data, loadMore));
    } else {
      yield put(actions.timelineAlreadyFetched());
    }
  } catch ({
    response
  }) {
    yield put(actions.timelineFailure());
  }
}

export default function* () {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount

  yield takeLatest(TIMELINE_REQUEST, getTimelines);
}
