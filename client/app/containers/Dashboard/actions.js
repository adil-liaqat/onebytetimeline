import {
  TIMELINE_REQUEST,
  TIMELINE_REQUEST_SUCCESS,
  TIMELINE_REQUEST_FAILURE,
  TIMELINE_ALREADY_FETCHED
} from './constants';

export function timelineRequest(start = 0, loadMore = false) {
  return {
    type: TIMELINE_REQUEST,
    start,
    loadMore
  };
}

export function timelineSuccess(data, loadMore = false) {
  return {
    type: TIMELINE_REQUEST_SUCCESS,
    data,
    loadMore
  };
}
export function timelineAlreadyFetched() {
  return {
    type: TIMELINE_ALREADY_FETCHED
  };
}

export function timelineFailure() {
  return {
    type: TIMELINE_REQUEST_FAILURE
  };
}
