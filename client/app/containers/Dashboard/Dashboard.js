/*
 * Dashboard
 *
 * List all the features
 */
import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import Timeline from '../../components/Timeline';
import TimelineItem from '../../components/TimelineItem';
import Button from '../../components/Button';
import NoDataMessage from '../../components/NoDataMessage';


export default class Dashboard extends PureComponent {
  state = {
    loadMore: false
  }
  componentDidMount() {
    const { getTimelines } = this.props;
    getTimelines();
  }

  onLoadMore = () => {
    this.setState({
      loadMore: true
    });
    const { timelines: { data = [] }, getTimelines } = this.props;
    getTimelines(data.length, true);
  }

  render() {
    const { timelines: { data = [], totalRecords }, loading } = this.props;
    const { loadMore } = this.state;
    if (loadMore && !loading) {
      this.setState({
        loadMore: false
      });
    }
    return (
      <Fragment>
        <Helmet>
          <title>Timeline</title>
        </Helmet>
        { !loadMore && (loading || !data.length) ?
          <NoDataMessage isLoading={loading} className="mt-4" message="No event Available" /> :
          <Fragment>
            <Timeline>
              { data.map(({ _id: id, ...rest }) => <TimelineItem key={id} {...rest} />) }
            </Timeline>
            { data.length < totalRecords &&
              <div className="row mb-3 flex-column align-items-center border-top border-secondary pt-2">
                <Button
                  text="Load more"
                  color="dark"
                  type="button"
                  isLoading={loadMore}
                  onClick={this.onLoadMore}
                />
              </div>
            }

          </Fragment>
        }
      </Fragment>
    );
  }
}

Dashboard.propTypes = {
  loading: PropTypes.bool,
  getTimelines: PropTypes.func,
  timelines: PropTypes.shape({
    data: PropTypes.array,
    totalRecords: PropTypes.number
  })
};
