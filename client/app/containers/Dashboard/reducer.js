import { fromJS } from 'immutable';
import {
  TIMELINE_REQUEST,
  TIMELINE_REQUEST_FAILURE,
  TIMELINE_REQUEST_SUCCESS,
  TIMELINE_ALREADY_FETCHED
} from './constants';
import { ADD_TIMELINE_REQUEST_SUCCESS, ADD_TIMELINE_REQUEST, ADD_TIMELINE_REQUEST_FAILURE } from '../AddTimeline/constants';

const initialState = fromJS({
  list: null,
  loading: false,
  fetched: false
});


export default function getDrivers(state = initialState, action) {
  switch (action.type) {
    case ADD_TIMELINE_REQUEST:
    case TIMELINE_REQUEST:
      return state.set('loading', true);
    case ADD_TIMELINE_REQUEST_FAILURE:
    case TIMELINE_ALREADY_FETCHED:
      return state.set('loading', false);
    case TIMELINE_REQUEST_SUCCESS:
      return state.set('loading', false).set('fetched', true).update('list', (list) => {
        if (!action.loadMore) {
          return action.data;
        }
        return {
          ...action.data,
          data: [...list.data, ...action.data.data]
        };
      });
    case TIMELINE_REQUEST_FAILURE:
      return state.set('list', []).set('loading', false).set('fetched', false);
    case ADD_TIMELINE_REQUEST_SUCCESS:
      return state.set('loading', false).update('list', (list) => ({
        ...list,
        totalRecords: list.totalRecords + 1,
        data: [action.data, ...list.data]
      }));
    default:
      return state;
  }
}
