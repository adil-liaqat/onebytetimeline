import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';

import reducer from './reducer';
import saga from './saga';

import { makeSelectTimeline, makeSelectTimelineLoading } from './selectors';

import { timelineRequest } from './actions';

import Dashboard from './Dashboard';

const mapDispatchToProps = (dispatch) => ({
  getTimelines: (start, loadMore) => dispatch(timelineRequest(start, loadMore)),
});

const mapStateToProps = createStructuredSelector({
  timelines: makeSelectTimeline(),
  loading: makeSelectTimelineLoading()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'timeline', reducer });
const withSaga = injectSaga({ key: 'timeline', saga });

export default compose(withReducer, withSaga, withConnect)(Dashboard);
