import { createSelector } from 'reselect';

const selectTimeline = (state) => state.get('timeline');

const makeSelectTimeline = () => createSelector(
  selectTimeline,
  (timeline) => (timeline.get('list') || {})
);

const makeSelectTimelineFetched = () => createSelector(
  selectTimeline,
  (timeline) => {
    if (typeof (timeline || {}).get === 'function') {
      return timeline.get('fetched');
    }
    return false;
  }
);
const makeSelectTimelineLoading = () => createSelector(
  selectTimeline,
  (timeline) => {
    if (typeof (timeline || {}).get === 'function') {
      return timeline.get('loading');
    }
    return false;
  }
);

export {
  makeSelectTimeline,
  makeSelectTimelineFetched,
  makeSelectTimelineLoading
};
