import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import { makeSelectAuthenticated } from '../Login/selectors';

const PrivateRouting = ({
  isAuthenticated,
  component: Component,
  layout: Layout,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      (isAuthenticated ? (
        <Layout>
          <Component {...props} />
        </Layout>
      ) : (
        <Redirect to={'/login'} />
      ))
    }
  />
);

const PublicRouting = ({
  isAuthenticated,
  component: Component,
  layout: Layout,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      (!isAuthenticated ? (
        <Layout>
          <Component {...props} />
        </Layout>
      ) : (
        <Redirect to={'/'} />
      ))
    }
  />
);


PrivateRouting.propTypes = {
  isAuthenticated: PropTypes.bool,
  component: PropTypes.func,
  layout: PropTypes.func
};
PublicRouting.propTypes = {
  isAuthenticated: PropTypes.bool,
  component: PropTypes.func,
  layout: PropTypes.func
};
const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectAuthenticated()
});

export const PrivateRoute = connect(mapStateToProps)(PrivateRouting);
export const PublicRoute = connect(mapStateToProps)(PublicRouting);
