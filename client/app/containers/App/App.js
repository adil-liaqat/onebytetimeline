/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch } from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr';

import NotFoundPage from 'containers/NotFoundPage/Loadable';

import Dashboard from 'containers/Dashboard';
import Login from 'containers/Login';
import Register from 'containers/Register';
import AddTimeline from 'containers/AddTimeline';

import LoginLayout from 'hoc/LoginLayout';
// import EmptyLayout from 'hoc/EmptyLayout';
import MainLayout from 'hoc/MainLayout';


// import Footer from 'components/Footer';
import './_style.scss';
import { PrivateRoute, PublicRoute } from './routes';

const App = () => (
  <div className="app-wrapper">
    <Helmet
      titleTemplate="%s"
      defaultTitle=""
    >
      <meta name="description" content="" />
    </Helmet>
    <ReduxToastr
      timeOut={4000}
      newestOnTop={false}
      preventDuplicates
      position="top-right"
      transitionIn="fadeIn"
      transitionOut="fadeOut"
      progressBar
      closeOnToastrClick
    />
    <Switch>

      <PublicRoute exact path="/login" layout={LoginLayout} component={Login} />
      <PublicRoute exact path="/register" layout={LoginLayout} component={Register} />

      <PrivateRoute exact path="/" layout={MainLayout} component={Dashboard} />
      <PrivateRoute exact path="/add" layout={MainLayout} component={AddTimeline} />

      <PrivateRoute path="*" layout={MainLayout} component={NotFoundPage} />
    </Switch>
  </div>
);

export default App;
