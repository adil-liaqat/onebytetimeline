import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form/immutable';
import { Helmet } from 'react-helmet';
import { InputField, TextAreaField } from '../../components/Fields';
import Button from '../../components/Button';

class AddTimeline extends Component {
  componentDidMount() {

  }

  render() {
    const {
      onSubmitForm, handleSubmit, dirty, submitting, loading
    } = this.props;
    return (
      <div className="row mt-5 flex-column align-items-center">
        <Helmet>
          <title>Add Timeline</title>
        </Helmet>
        <div className="col-4">
          <div className="card">
            <h5 className="card-header">Add new Event</h5>
            <div className="card-body">
              <form className="form-horizontal" onSubmit={handleSubmit(onSubmitForm)}>
                <div className="form-group">

                  <Field
                    name="title"
                    id="title"
                    type="text"
                    component={InputField}
                    label="Title"
                  />
                </div>
                <div className="form-group">

                  <Field
                    name="description"
                    id="description"
                    type="text"
                    component={TextAreaField}
                    label="Description"
                  />
                </div>
                <div className="form-group last">

                  <Button
                    text="Submit"
                    className="float-right"
                    isDisabled={submitting || !dirty}
                    isLoading={loading}
                  />
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddTimeline;
