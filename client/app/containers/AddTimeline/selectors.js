import { createSelector } from 'reselect';

const selectTimelineForm = (state) => (state.get('form').toJS() || {}).addTimeline;

const makeSelectFormValues = () => createSelector(
  selectTimelineForm,
  (form) => ({ ...form.values })
);

export { makeSelectFormValues };
