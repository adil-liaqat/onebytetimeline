import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects';


import { push } from 'react-router-redux';
import { toastr } from 'react-redux-toastr';
import axios from 'axios';

import { ADD_TIMELINE_REQUEST } from './constants';

import * as actions from './actions';
import { makeSelectFormValues } from './selectors';

export function* addTimeline() {
  const requestURL = '/api/v1/en/user/timeline';

  try {
    // Call our request helper (see 'utils/request')
    const form = yield select(makeSelectFormValues());
    const {
      data
    } = yield call(axios.post, requestURL, form);


    yield put(actions.addTimelineSuccess(data));
    toastr.success('Success', 'Event added Successfully!');
    yield put(push('/'));
  } catch ({
    response
  }) {
    toastr.error('Error', response.data || 'Something went wrong!');
    yield put(actions.addTimelineFailure());
  }
}

export default function* () {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount

  yield takeLatest(ADD_TIMELINE_REQUEST, addTimeline);
}
