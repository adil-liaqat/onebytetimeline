import {
  ADD_TIMELINE_REQUEST,
  ADD_TIMELINE_REQUEST_SUCCESS,
  ADD_TIMELINE_REQUEST_FAILURE,
} from './constants';

export function addTimelineRequest() {
  return {
    type: ADD_TIMELINE_REQUEST,
  };
}

export function addTimelineSuccess(data) {
  return {
    type: ADD_TIMELINE_REQUEST_SUCCESS,
    data,
  };
}


export function addTimelineFailure() {
  return {
    type: ADD_TIMELINE_REQUEST_FAILURE
  };
}
