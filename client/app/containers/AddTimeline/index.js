import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm } from 'redux-form/immutable';

import AddTimeline from './AddTimeline';
import injectSaga from '../../utils/injectSaga';

import saga from './saga';
import { addTimelineRequest } from './actions';
import { makeSelectTimelineLoading } from '../Dashboard/selectors';

const validate = (values) => {
  values = values.toJS();
  const errors = {};
  // VALIDATION FOR INPUT FIELDS
  if (!values.title || !values.title.trim()) {
    errors.title = 'Title is required';
  }
  if (!values.description || !values.description.trim()) {
    errors.description = 'Description is required';
  }
  return errors;
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectTimelineLoading()
});

const mapDispatchToProps = (dispatch) => ({
  onSubmitForm: () => dispatch(addTimelineRequest())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'addTimeline', saga });

const LoginForm = reduxForm({
  form: 'addTimeline',
  enableReinitialize: true,
  validate
});

export default compose(withConnect, withSaga, LoginForm)(AddTimeline);
