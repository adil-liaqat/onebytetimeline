import {
  REGISTER_REQUEST,
  REGISTER_REQUEST_SUCCESS,
  REGISTER_REQUEST_FAILURE,
} from './constants';

export function registerRequest() {
  return {
    type: REGISTER_REQUEST,
  };
}

export function registerSuccess(user) {
  return {
    type: REGISTER_REQUEST_SUCCESS,
  };
}

export function registerFailure() {
  return {
    type: REGISTER_REQUEST_FAILURE,
  };
}
