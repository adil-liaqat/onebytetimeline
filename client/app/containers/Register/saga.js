import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects';

import {
  push
} from 'react-router-redux';

import axios from 'axios';
import {
  toastr} from 'react-redux-toastr';

import {
  REGISTER_REQUEST} from './constants';

import * as actions from './actions';
import {
  makeSelectFormValues
} from './selectors';

export function* register() {
  const requestURL = '/api/v1/en/user/register';

  try {
    // Call our request helper (see 'utils/request')
    const form = yield select(makeSelectFormValues());
    const {
      data
    } = yield call(axios.post, requestURL, form);

    yield put(actions.registerSuccess());
    toastr.success('Success', 'Account created successfully');
    yield put(push('/login'));
  } catch ({
    response
  }) {
    let error = 'Something went wrong!';
    if (typeof response.data === 'string') {
      error = response.data;
    }
    toastr.error('Error', error);
    yield put(actions.registerFailure());
  }
}

export default function* registerRequest() {
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount

  yield takeLatest(REGISTER_REQUEST, register);
}
