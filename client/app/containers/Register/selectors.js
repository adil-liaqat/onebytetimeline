import {
  createSelector
} from 'reselect';

const selectRegisterForm = (state) => (state.get('form').toJS() || {}).registerForm;

const makeSelectFormValues = () => createSelector(
  selectRegisterForm,
  (form) => ({
    ...form.values
  })
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

export { makeSelectFormValues };
