import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form/immutable';
import { InputField } from '../../components/Fields';
import Button from '../../components/Button';

class Register extends Component {
  componentDidMount() {

  }
  render() {
    const {
      onSubmitForm, handleSubmit, dirty, submitting, loading
    } = this.props;
    return (
      <div className="card">
        <h5 className="card-header">Register</h5>
        <div className="card-body">
          <form className="form-horizontal" onSubmit={handleSubmit(onSubmitForm)}>
            <div className="form-group">

              <Field
                name="firstname"
                id="firstname"
                type="text"
                component={InputField}
                label="First Name"
              />

            </div>
            <div className="form-group">

              <Field
                name="lastname"
                id="lastname"
                type="text"
                component={InputField}
                label="Last Name"
              />

            </div>
            <div className="form-group">

              <Field
                name="username"
                id="username"
                type="text"
                component={InputField}
                label="Email"
              />

            </div>
            <div className="form-group">

              <Field
                name="password"
                id="password"
                type="password"
                component={InputField}
                label="Password"
              />

            </div>
            <div className="form-group last">

              <Button
                text="Register"
                className="float-right"
                isDisabled={submitting || !dirty}
                isLoading={loading}
              />
            </div>
          </form>

        </div>
        <div className="card-footer">
          Already registered? <Link to="/login" className>Sign In</Link>
        </div>
      </div>
    );
  }
}

export default Register;
