import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';

import injectSaga from '../../utils/injectSaga';

import Register from './Register';
import { registerRequest } from './actions';

import saga from './saga';
import { makeSelectLoading } from '../Login/selectors';

const validate = (values) => {
  values = values.toJS();
  const errors = {};
  if (!values.firstname || !values.firstname.trim()) {
    errors.firstname = 'First name is required';
  }
  if (!values.lastname || !values.lastname.trim()) {
    errors.lastname = 'Last name is required';
  }
  if (!values.username || !values.username.trim()) {
    errors.username = 'Username is required';
  }
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.username)) {
    errors.username = 'Invalid Email';
  }
  if (!values.password || !values.password.trim()) {
    errors.password = 'Password is required';
  }
  return errors;
};

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
});

const mapDispatchToProps = (dispatch) => ({
  onSubmitForm: () => dispatch(registerRequest())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);


const withSaga = injectSaga({ key: 'register', saga });

const RegisterForm = reduxForm({
  form: 'registerForm',
  enableReinitialize: true,
  validate
});

export default compose(withConnect, withSaga, RegisterForm)(Register);
