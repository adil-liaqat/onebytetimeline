import React from 'react';

const LoginLayout = (props) => (
  <div className="container">
    <div className="row flex-column align-items-center mt-5">
      <div className="col-4">
        { props.children }
      </div>
    </div>
  </div>
)

export default LoginLayout;

