import React, { Component, Fragment } from 'react';
import './_style.scss';
import Navbar from '../../components/Navbar';

class MainLayout extends Component {

  componentDidMount() {
    this.props.checkToken();
  }


  render() {
    const { user, logout } = this.props;
    return (
      <Fragment>
        <Navbar user={user} logout={logout} />
        <div className="container-fluid">
          {this.props.children}
        </div>
      </Fragment>
    );
  }
}

export default MainLayout;
