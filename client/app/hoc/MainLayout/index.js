import { connect } from 'react-redux';
import { compose } from 'redux';
import { translate } from 'react-i18next';
import { createStructuredSelector } from 'reselect';

import MainLayout from './MainLayout';

import {
  checkToken,
  logout
} from '../../containers/Login/actions';

import { makeSelectCurrentUser } from '../../containers/Login/selectors';

const mapStateToProps = createStructuredSelector({
  user: makeSelectCurrentUser()
});

const mapDispatchToProps = (dispatch) => ({
  checkToken: () => dispatch(checkToken()),
  logout: () => dispatch(logout())
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withTranslate = translate();

export default compose(withTranslate, withConnect)(MainLayout);
