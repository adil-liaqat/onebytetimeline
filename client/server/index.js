/* eslint consistent-return:0 */

const express = require('express');
const logger = require('./util//logger');

const argv = require('./util/argv');
const port = require('./util//port');
const setup = require('./middlewares/frontendMiddleware');
const {
  resolve,
  join
} = require('path');
const {
  readFileSync,
  writeFileSync
} = require('fs');

var bodyParser = require('body-parser');

const app = express();

// If you need a backend, e.g. an API, add your custom backend-specific middleware here
// app.use('/api', myApi);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(express.static(join(__dirname, '..', 'app', 'public')));

// In production we need to pass these values in instead of relying on webpack
setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
});

// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';

if (process.env.NODE_ENV === 'development') {
  app.post('/locale/add/:lng/:ns', (req, res) => {
    delete req.body._t;
    const filePath = resolve(__dirname, '..', 'app', 'public', 'locale', req.params.lng, req.params.ns + '.json');
    const body = req.body || {};
    let data = readFileSync(filePath, 'utf8');
    data = JSON.parse(data);
    for (const iterator in body) {
      data[iterator] = body[iterator]
    }
    data = JSON.stringify(data, null, 2);
    writeFileSync(filePath, data);

    return res.json(data);
  });
}
// Start your app.
app.listen(port, host, (err) => {
  if (err) {
    return logger.error(err.message);
  }
  logger.appStarted(port, prettyHost);
});
