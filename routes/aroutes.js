const path = require('path');
const serveStatic = require('serve-static');
const publicPath = path.resolve(__dirname, '..', 'client', 'build');

module.exports = {
  'GET /*': serveStatic(publicPath, { skipAssets: true }),
  'POST /api/v1/en/user/login': 'user/login',
  'POST /api/v1/en/user/register': 'user/register',
  'GET /api/v1/en/user/timeline': 'timeline/get-timeline',
  'POST /api/v1/en/user/timeline': 'timeline/create-timeline',
  'GET *': (req, res) => res.sendFile(path.join(publicPath, 'index.html')),
};
