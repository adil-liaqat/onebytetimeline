const supertest = require('supertest');
const { expect } = require('chai');
const { pick } = require('lodash');

let token = null;

describe('UserController.login', function() {

  describe('#register()', function() {
    it('should insert user', function (done) {
      supertest(sails.hooks.http.app)
      .post('/api/v1/en/user/register')
      .send({ username: 'test@test.com', password: '123', firstname: 'Hello', lastname: "test" })
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(response => {
        expect(response.body.email).to.equal('test@test.com')
        expect(response.body.firstName).to.equal('hello')
        expect(response.body.lastName).to.equal('test')
      })
      .end(done);
    });
  });

  describe('#login()', function() {
    it('should return user object', function (done) {
      supertest(sails.hooks.http.app)
      .post('/api/v1/en/user/login')
      .send({ username: 'test@test.com', password: '123' })
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(response => {
        expect(response.body.user.email).to.equal('test@test.com')
        token = response.body.token;
      })
      .end(done);
    });
  });

  describe('#createEvent()', function() {
    it('should insert new event', function (done) {
      supertest(sails.hooks.http.app)
      .post('/api/v1/en/user/timeline')
      .set('Authorization', 'Bearer ' + token)
      .send({ title: 'New Event', description: 'Test Event' })
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(response => {
        expect(response.body.title).to.equal('New Event')
        expect(response.body.description).to.equal('Test Event')
      })
      .end(done);
    });
  });

  describe('#getTimeline()', function() {
    it('should return timline', function (done) {
      supertest(sails.hooks.http.app)
      .get('/api/v1/en/user/timeline')
      .set('Authorization', 'Bearer ' + token)
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(response => {
        expect(response.body.start).to.equal(0)
        expect(response.body.totalRecords).to.equal(1)
        expect(response.body.data[0].title).to.equal('New Event')
        expect(response.body.data[0].description).to.equal('Test Event')
      })
      .end(done);
    });
  });

});
