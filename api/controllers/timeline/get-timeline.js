module.exports = {
  friendlyName: "Get timeline",

  description: "Get user timeline",

  inputs: {
    limit: {
      description: 'Total number of records.',
      type: "number",
      defaultsTo: 5
    },
    start: {
      description: 'Start position of getting records.',
      type: "number",
      defaultsTo: 0
    }
  },

  exits: {
    exception: {}
  },

  fn: async function ({ limit, start }, exits) {
    try {
      const {
        _id
      } = this.req.user;

      const [{
        data: [ { timeline = [] } = {} ] = [],
        total: [ { count = 0 } = {} ] = []
      }] = await User.aggregate([
        {"$facet": {
          "data": [
            { $match: { _id } },
            { $unwind: "$timeline" },
            { $sort : { "timeline.createdAt" : -1 } },
            { $group : { _id: "$_id", timeline: { $push: "$timeline" } } },
            { $project: { "timeline" : { "$slice": [ "$timeline", start, limit ] } } },
          ],
          "total": [
            { $match: { _id } },
            { $unwind: "$timeline" },
            { $count: "count" }
          ]
        }}
      ]);

      exits.success({
        start,
        totalRecords: count,
        limit,
        data: timeline
      });
    } catch (ex) {
      return exits.exception({
        status: 500,
        message: ex.message
      });
    }
  }
};
