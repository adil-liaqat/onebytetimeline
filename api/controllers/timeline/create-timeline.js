module.exports = {
  friendlyName: "Create timeline",

  description: "Create user timeline",

  inputs: {
    title: {
      description: 'title of the timeline".',
      type: "string",
      required: true
    },

    description: {
      description: 'description of the timeline.',
      type: "string",
      required: true
    }
  },

  exits: {
    exception: {}
  },

  fn: async function({ title , description }, exits) {
    try {
      const { id } = this.req.user;
      var addTimeline = await User.findOneAndUpdate({_id: id}, {
        $push: {
          timeline: {
            title,
            description
          }
        }
      }, {
        fields: { 'timeline': 1, '_id':0 },
        new: true
      }).lean();

      return exits.success(addTimeline.timeline.pop());
    } catch (ex) {
      // return exits.exception(parse(ex.message));
      return exits.exception({
        status: 500,
        message: ex.message
      });
    }
  }
};
