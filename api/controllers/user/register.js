module.exports = {


  friendlyName: 'Register',


  description: 'Register user.',


  inputs: {
    username: {
      description: 'The email to try in this attempt, e.g. "irl@example.com".',
      type: 'string',
      required: true,
      isEmail: true
    },
    password: {
      description: 'password',
      type: 'string',
      required: true
    },
    firstname: {
      description: 'first name',
      type: 'string',
      required: true
    },
    lastname: {
      description: 'last name',
      type: 'string',
      required: true
    }
  },


  exits: {
    exception: {
      responseType: 'exception'
    },
    success: {
      responseType: 'success'
    }
  },

  // Add User
  fn: async function (inputs, exits) {
    try {
      var createdUser = await User.create({
        firstName: inputs.firstname,
        lastName: inputs.lastname,
        email: inputs.username,
        password: inputs.password
      });
      createdUser = createdUser.toJSON();

      return exits.success(createdUser);
    } catch (ex) {
      return exits.exception({
        status: 500,
        message: ex.message
      });
    }
  }
};
