const validator = require('validator');

module.exports = {

  schema: {
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      validate: {
        isAsync: true,
        validator: validator.isEmail,
        message: '{VALUE} is not a valid email'
      }
    },
    firstName: {
      type: String,
      trim: true,
      lowercase: true
    },
    lastName: {
      type: String,
      trim: true,
      lowercase: true
    },
    createdAt: {
      type: Date,
      default: Date.now
    },
    updatedAt: {
      type: Date,
      default: Date.now
    },
    password: {
      type: String,
      required: true
    },
    timeline: [
      {
        title: {
          type: String,
          trim: true,
          required: true
        },
        description: {
          type: String,
          trim: true,
          required: true
        },
        createdAt: {
          type: Date,
          default: Date.now
        },
        updatedAt: {
          type: Date,
          default: Date.now
        },
      }
    ]
  },


  /**
   * constructSchema()
   *
   * Note that this function must be synchronous!
   *
   * @param  {Dictionary} schemaDefinedAbove  [the raw schema defined above, or `{}` if no schema was provided]
   * @param  {SailsApp} sails                 [just in case you have globals disabled, this way you always have access to `sails`]
   * @return {MongooseSchema}
   */
  constructSchema: function (schemaDefinedAbove, sails) {
    // e.g. we might want to pass in a second argument to the schema constructor
    var UserSchema = new sails.mongoose.Schema(schemaDefinedAbove,{
        toObject: { virtuals: true }
    });

    // Or we might want to define an instance method:
    UserSchema.virtual('fullName').get(function () {
      var user = this;
      if(user.firstName && user.lastName){
        return user.firstName.toProperCase() + ' ' + user.lastName.toProperCase();
      }
      return user;
    });

    UserSchema.set('toJSON', {
      virtuals: true,
      transform: (doc, ret, options) => {
        return _.omit(ret, ['password', 'createdAt', 'updatedAt']);
      }
    });


    UserSchema.pre('save', async function (next) {
      var user = this;
      try {
        var exist = await User.findOne({
          email: user.email
        });
        if (!exist) {
          user.password = await sails.helpers.passwords.hashPassword(user.password);
        } else {
          throw new Error(sails.__('User already exists.'));

        }
        next();
      } catch (ex) {
        next(ex);
      }
    });

    // Regardless, you must return the instantiated Schema instance.
    return UserSchema;
  }

};
