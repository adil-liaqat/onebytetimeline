# Timeline App

Timeline App is a simple app, created with react.

## Quick start

1.  Make sure that you have Node.js v10 and npm v6 or above installed.
2.  Clone this repo using `git clone git@bitbucket.org:adil-liaqat/onebytetimeline.git <YOUR_PROJECT_NAME>`
3.  Move to the appropriate directory: `cd <YOUR_PROJECT_NAME>`.
4.  Run `npm run setup` in order to install dependencies.
    _At this point you can run `npm start` to see the example app at `http://localhost:1337`._

## Connection and Database

You only need to touch config/datastores.js, and change the `uri` for the mongodb.

Now simple configure credentials.

```js
{
  uri: 'mongodb://localhost:27017/timeline'
}
```


To start the DB, add the credentials for production. add `environment variables` by typing e.g. `export DB_URI=yourstring` before starting the app.

Now you're ready to go!

```sh
$ npm start
```
for test:
```sh
$ npm run test
```

for development:
```sh
$ npm run dev
```
