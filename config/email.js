module.exports.email = {
    service: "Hotmail",
    auth: {
        user: 'example@example.com',
        pass: ''
    },
    logger: false, // log to console
    debug: false,
    templateDir: "views/emails",
    from: '"Example" <example@example.com>',
    testMode: false,
    // ssl: true
}