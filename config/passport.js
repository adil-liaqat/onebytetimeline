const passport = require('passport'),
  JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt,
  LocalStrategy = require('passport-local').Strategy;

var EXPIRES_IN_MINUTES = 60 * 24;
var SECRET = process.env.tokenSecret || "AaBdDfezf221@!2231@#$%^%$";
var ALGORITHM = "HS256";
var ISSUER = "example.com";
var AUDIENCE = "abc@example.com";

passport.use(new LocalStrategy({
  usernameField: 'username',
  passportField: 'password'
}, async function (username, password, cb) {
  try {
    var user = await User.findOne({
        email: username
    }, '-timeline');

    if (!user) return cb(null, false, 'Invalid username');

    await sails.helpers.passwords.checkPassword(password, user.password)
      .intercept(() => 'Invalid Password');

    user = user.toJSON();

    return cb(null, user, {
      message: 'Login Successful.'
    });
  } catch (ex) {
    return cb(ex.message);
  }


}));

passport.use(
  new JwtStrategy({
    secretOrKey: SECRET,
    issuer: ISSUER,
    audience: AUDIENCE,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
  }, async function (payload, cb) {
    try {
      var user = await User.findOne({

          _id: payload.id

      }, '-timeline');

      //if (err) return cb(err);
      if (!user) return cb(null, false, {
        message: 'Username not found'
      });

      user = user.toJSON();


      return cb(null, user, {});
    } catch (ex) {
      return cb(ex.message);
    }
  }));


module.exports.jwtSettings = {
  expiresInMinutes: EXPIRES_IN_MINUTES,
  secret: SECRET,
  algorithm: ALGORITHM,
  issuer: ISSUER,
  audience: AUDIENCE
};
